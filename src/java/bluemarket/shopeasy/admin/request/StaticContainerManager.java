/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.request;

import be.bluemarket.shopeasy.database.models.Produit;
import be.bluemarket.shopeasy.database.models.Rayon;
import be.bluemarket.shopeasy.database.models.Recette;
import com.ibm.icu.text.Transliterator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.servlet.http.Part;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author Thomas Menini
 */
public class StaticContainerManager {

    public static String url = "http://192.168.128.13/~d120027/shopeasy/web/container";

    private String ask;
    private String delete;

    private StaticContainerManager(String ask, String delete) {
        // convertit les caractères accentuées en leur équivalent sans accent (url propre)
        Transliterator accentsConverter = Transliterator.getInstance("Any-Latin; NFD; [:M:] Remove; NFC; [^\\p{ASCII}] Remove");
        this.ask = accentsConverter.transliterate(ask).replaceAll(" ", "_");
        this.delete = accentsConverter.transliterate(delete).replaceAll(" ", "_");
    }

    public StaticContainerManager(Produit produit) {
        this(String.format(
                "%s/ask/path/produit/%s/%s",
                url,
                produit.getRayon().getNom(),
                produit.getNom()
        ), String.format(
                "%s/delete/produit/%s/%s",
                url,
                produit.getRayon().getNom(),
                produit.getNom()
        ));
    }

    public StaticContainerManager(Recette recette) {
        this(String.format(
                "%s/ask/path/recette/%s/%s",
                url,
                recette.getCategorie(),
                recette.getNom()
        ), String.format(
                "%s/delete/recette/%s/%s",
                url,
                recette.getCategorie(),
                recette.getNom()
        ));
    }

    public String enregistrer(Part image) throws Exception {
        HttpPost post = new HttpPost(ask);
        File file = this.store(image);
        FileBody filebody = new FileBody(file);

        HttpEntity entity = MultipartEntityBuilder.create()
                .addPart("image_file", filebody)
                .build();

        post.setEntity(entity);
        // add header
        // post.setHeader("User-Agent", USER_AGENT);

        String result = envoyer(post, 201);
        file.delete();
        return result;
    }

    public boolean supprimer() throws Exception {
        HttpGet get = new HttpGet(delete);
        String result = envoyer(get, 200);
        return result.equals("deleted");
    }

    private String envoyer(HttpUriRequest request, int statusAttendu) throws Exception {
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            String result = recevoir(client, request, statusAttendu);
            return result;
        }
    }

    private String recevoir(CloseableHttpClient client, HttpUriRequest request, int statusAttendu) throws Exception {
        try (CloseableHttpResponse response = client.execute(request)) {
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            if (response.getStatusLine().getStatusCode() != statusAttendu) {
                throw new Exception(result.toString());
            }
            return result.toString();
        }
    }

    private File store(Part p) throws IOException {
        String filepath = getFilepath(getFilename(p));

        InputStream input = p.getInputStream();
        try (FileOutputStream output = new FileOutputStream(filepath)) {
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while (true) {
                bytesRead = input.read(buffer);
                if (bytesRead <= 0) {
                    break;
                }
                output.write(buffer, 0, bytesRead);
            }
        }

        return new File(filepath);
    }

    private String getFilename(Part part) {
        String filename = "";
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String name = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                filename = name.substring(name.lastIndexOf('/') + 1).substring(name.lastIndexOf('\\') + 1);
            }
        }
        return filename;
    }

    private String getFilepath(String filename) {
        String path = "";
        File file = new File(filename);
        try (FileOutputStream output = new FileOutputStream(file)) {
            path = file.getAbsolutePath();
        } catch (FileNotFoundException ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return path;
    }
}
