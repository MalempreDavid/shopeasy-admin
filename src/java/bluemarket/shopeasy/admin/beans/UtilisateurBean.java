/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Utilisateurs;
import be.bluemarket.shopeasy.database.models.Utilisateur;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author David
 */
@ManagedBean
public class UtilisateurBean extends MasterBean {

    @PostConstruct
    public void init() {
        Utilisateurs.manager();
    }

    //Query: Select utilisateur ORDER BY pseudo
    public List<Utilisateur> getUtilisateurs() {
        return Utilisateurs.manager().findEntities(Utilisateur.Order.Login);
    }

}
