package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Produits;
import be.bluemarket.shopeasy.database.jpa.controllers.Rayons;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.IllegalOrphanException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Produit;
import be.bluemarket.shopeasy.database.models.Rayon;
import bluemarket.shopeasy.admin.request.StaticContainerManager;
import javax.faces.bean.ManagedBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;

@ManagedBean
@ViewScoped
public class ProduitBean extends MasterBean {

    private Produit produit;

    private Part image;

    @PostConstruct
    public void init() {
        Produits.manager();
        produit = new Produit();
        produit.setRayon(new Rayon());
    }

    public Produit getProduit() {
        return produit;
    }

    //Call static container           
    //Query sauvegarder produit
    public String ajouter() {
        if (produit.getNom().isEmpty()) {
            super.setErreur("Le nom est vide");
            return null;
        }
        if (produit.getPrix() <= 0) {
            super.setErreur("Le prix ne peut être nul ou négatif");
            return null;
        }
        if(image == null) {
            super.setErreur("Le fichier image doit être fourni");
            return null;
        }

        try {
            Rayon r = Rayons.manager().find(produit.getRayon().getId());
            produit.setRayon(r);
            String url = askUrl(image);
            produit.setUrlImage(url);
            Produits.manager().create(produit);
            return "produit";
        } catch (Exception ex) {
            Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'ajout a échoué");
            return null;
        }
    }

    private String askUrl(Part image) throws Exception {
        StaticContainerManager scm = new StaticContainerManager(produit);
        String url = scm.enregistrer(image);
        image.delete();
        return url;
    }

    public Part getFile() {
        return image;
    }

    public void setFile(Part file) {
        image = file;
    }

    //Query: delete produit where id = super.getIdADelete()
    public void delete() {
        try {
            Produit p = Produits.manager().find(super.getIdADelete());
            StaticContainerManager scm = new StaticContainerManager(p);
            if (scm.supprimer()) {
                Produits.manager().destroy(super.getIdADelete());
            }
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Produit> getProduits() {
        return Produits.manager().findEntities();
    }

}
