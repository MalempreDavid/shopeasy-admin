/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans.edit;

import be.bluemarket.shopeasy.database.jpa.controllers.Utilisateurs;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Utilisateur;
import bluemarket.shopeasy.admin.beans.MasterBean;
import bluemarket.shopeasy.admin.beans.RayonBean;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author David
 */
@ManagedBean
public class EditUtilisateurBean extends MasterBean {

    private Utilisateur utilisateur = new Utilisateur();

    @PostConstruct
    public void init() {
        Utilisateurs.manager();
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String value = req.getParameter("id");
        super.setId(Integer.valueOf(value));

        setAllFieldEdition();
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public String[] getRoles() {
        String[] roles = new String[2];
        roles[0] = "Client";
        roles[1] = "Administrateur";
        return roles;
    }

    //Query: select * from utilisateur where id = utilisateur.id
    public void setAllFieldEdition() {
        try {
            utilisateur = Utilisateurs.manager().find(super.getId());

            if (utilisateur == null) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.getExternalContext().redirect("utilisateur.xhtml");
            }

        } catch (IOException ex) {
            Logger.getLogger(EditUtilisateurBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Query: upate utilisasteur where id = utilisateur.id
    public String update() {

        if (utilisateur.getLogin().isEmpty()
                || utilisateur.getNom().isEmpty()
                || utilisateur.getPrenom().isEmpty()) {
            super.setErreur("Un ou plusieurs champs sont vides");
            return null;
        }

        Utilisateur utilisateurQuery = Utilisateurs.manager().find(utilisateur.getId());
        //insertion dans les champs
        utilisateurQuery.setNom(utilisateur.getNom());
        utilisateurQuery.setPrenom(utilisateur.getPrenom());
        utilisateurQuery.setLogin(utilisateur.getLogin());

        if (utilisateur.getRole() != null && !utilisateur.getRole().equals("SuperAdmin")) {
            utilisateurQuery.setRole(utilisateur.getRole());
        }

        try {
            Utilisateurs.manager().edit(utilisateurQuery);
            return "utilisateur";
        } catch (NonexistentEntityException ex) {
            // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, "Cet utilisateur n'existe pas (ou plus), il ne peut être édité", ex);
            super.setErreur("Cet utilisateur n'existe pas (ou plus), il ne peut être édité");
            return null;
        } catch (Exception ex) {
            Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, "L'édition a échouée", ex);
            super.setErreur("L'édition a échouée");
            return null;
        }
    }
}
