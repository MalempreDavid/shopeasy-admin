package bluemarket.shopeasy.admin.beans.edit;

import bluemarket.shopeasy.admin.beans.*;
import be.bluemarket.shopeasy.database.jpa.controllers.Rayons;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Rayon;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
@ViewScoped
public class EditRayonBean extends MasterBean {

    private Rayon rayon = new Rayon();

    
     @PostConstruct
    private void init() {
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String value = req.getParameter("id");
        super.setId(Integer.valueOf(value));

        setAllFieldEdition();
    }

    public Rayon getRayon() {
        return rayon;
    }

    //Query update rayon  where id=rayon.idRayon
    public String update() {
        if (rayon.getNom().isEmpty()) {
            super.setErreur("Le nom ne peut être vide");
            return null;
        }

        try {
            Rayons.manager().edit(rayon);
            return "rayon";
        } catch (NonexistentEntityException ex) {
            // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("Ce rayon n'existe pas (ou plus), il ne peut être édité");
            return null;
        } catch (Exception ex) {
            // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'édition a échouée");
            return null;
        }
    }


    //Query: get rayon where id= rayon.id
    public void setAllFieldEdition() {
        rayon = Rayons.manager().find(super.getId());

        if (rayon == null) {
            try {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.getExternalContext().redirect("rayon.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
