package bluemarket.shopeasy.admin.beans.edit;

import bluemarket.shopeasy.admin.beans.*;
import be.bluemarket.shopeasy.database.jpa.controllers.Produits;
import be.bluemarket.shopeasy.database.jpa.controllers.Rayons;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Produit;
import be.bluemarket.shopeasy.database.models.Rayon;
import bluemarket.shopeasy.admin.request.StaticContainerManager;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

@ManagedBean
@ViewScoped
public class EditProduitBean extends MasterBean {

    private Produit produit;

    private Part image;

    @PostConstruct
    public void init() {
        Produits.manager();
        produit = new Produit();
        produit.setRayon(new Rayon());
         
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String value = req.getParameter("id");
        super.setId(Integer.valueOf(value));

        setAllFieldEdition();
    }

    public Produit getProduit() {
        return produit;
    }

    private String askUrl(Part image) throws Exception {
        StaticContainerManager scm = new StaticContainerManager(produit);
        String url = scm.enregistrer(image);
        image.delete();
        return url;
    }

    public Part getFile() {
        return image;
    }

    public void setFile(Part file) {
        image = file;
    }



    public List<Produit> getProduits() {
        return Produits.manager().findEntities();
    }

    //Query get produit where id = produit.idProduit
    public void setAllFieldEdition() {
        produit = Produits.manager().find(super.getId());

        if (produit == null) {
            try {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.getExternalContext().redirect("produit.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(EditProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Query update produit en m�moire (produit)
    public String update() {
        if (produit.getNom().isEmpty()) {
            super.setErreur("Le nom est vide");
            return null;
        }
        if (produit.getPrix() <= 0) {
            super.setErreur("Le prix ne peut être nul ou négatif");
            return null;
        }

        try {
            Rayon r = Rayons.manager().find(produit.getRayon().getId());
            produit.setRayon(r);
            String url = askUrl(image);
            produit.setUrlImage(url);
            Produits.manager().edit(produit);
        } catch (NonexistentEntityException ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("Le produit n'existe pas (ou plus)");
            return null;
        } catch (Exception ex) {
            // Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("La modification a échouée");
            return null;
        }

        return "produit";
    }

}
