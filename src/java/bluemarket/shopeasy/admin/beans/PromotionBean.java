/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Produits;
import be.bluemarket.shopeasy.database.jpa.controllers.Promotions;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Produit;
import be.bluemarket.shopeasy.database.models.Promotion;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author David
 */
@ManagedBean
@ViewScoped
public class PromotionBean extends MasterBean {

    private Promotion promotion;
    private int idProduit;

      
    @PostConstruct
    public void init() {
        Promotions.manager();
        Produits.manager();
        promotion = new Promotion();
    }

    public Promotion getPromotion() {
        return promotion;
    }

    //Query get all produit sans promo
    public List<Produit> getListeProduitSansPromo() {
        return Produits.manager().getEntitiesByPromotion(null);
    }

    public List<Promotion> getPromotions() {
        return Promotions.manager().findEntities();
    }
    
    //Query: delete where id = idPromotionASupprimer
    public void delete(Integer idPromotion) {
        try {
            Promotions.manager().destroy(idPromotion);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PromotionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Query: insert promotion
    public String ajouterPromotion() {
        if (promotion.getPourcentage() <= 0) {
            super.setErreur("Le pourcentage ne peut être inférieur ou égal à 0");
            return null;
        }

        try {
            List<Produit> produits = new ArrayList<>();
            Produit produitAAjouter = new Produit(idProduit);
            produits.add(produitAAjouter);
            promotion.setProduits(produits);
            Promotions.manager().create(promotion);
            return "promotion";
        } catch (Exception ex) {
            Logger.getLogger(PromotionBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'ajout a échoué");
            return null;
        }
    }

    /**
     * @return the idProduit
     */
    public int getIdProduit() {
        return idProduit;
    }

    /**
     * @param idProduit the idProduit to set
     */
    public void setIdProduit(int idProduit) {
        this.idProduit = idProduit;
    }
}
