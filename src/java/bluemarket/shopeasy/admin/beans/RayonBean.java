package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Rayons;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.IllegalOrphanException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.WrongAttributeNameException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.WrongAttributeTypeException;
import be.bluemarket.shopeasy.database.models.Rayon;
import javax.faces.bean.ManagedBean;
import java.util.List;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class RayonBean extends MasterBean {

    private Rayon rayon = new Rayon();

    public Rayon getRayon() {
        return rayon;
    }
    
    //Query: SELECT MAX(position) FROM rayon
    public String ajouter() {
        if (rayon.getNom().isEmpty()) {
            super.setErreur("Erreur: le champs nom est vide");
            return null;
        }

        int position = Rayons.manager().max(Integer.class, "position");
        rayon.setPosition(position + 1);

        if (rayon == null) {
            return null;
        }
        try {
            Rayons.manager().create(rayon);
            return "rayon";
        } catch (Exception ex) {
            // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'ajout a échoué");
        }
        return null;
    }

    public List<Rayon> getRayons() {
        return Rayons.manager().findEntities();
    }

    //Query: Update rayon where position-1 set position = position
    //Query: Update rayon where id=idRayon set position = position-1
    public void monter(int idRayon, int position) {
        Rayon descendant, montant;
        try {
            descendant = Rayons.manager().find("position", position - 1);
            descendant.setPosition(position);

            montant = Rayons.manager().find(idRayon);
            montant.setPosition(position - 1);
        } catch (WrongAttributeTypeException | WrongAttributeNameException ex) {
            descendant = null;
            montant = null;
        }

        if (descendant != null) {
            try {
                Rayons.manager().edit(descendant);
                Rayons.manager().edit(montant);
            } catch (NonexistentEntityException ex) {
                // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Query: Update rayon where position+1 set position = position
    //Query: Update rayon where id=idRayon set position = position+1
    public void descendre(int idRayon, int position) {
        Rayon descendant, montant;
        try {
            montant = Rayons.manager().find("position", position + 1);
            montant.setPosition(position);

            descendant = Rayons.manager().find(idRayon);
            descendant.setPosition(position + 1);
        } catch (WrongAttributeTypeException | WrongAttributeNameException ex) {
            descendant = null;
            montant = null;
        }

        if (descendant != null) {
            try {
                Rayons.manager().edit(descendant);
                Rayons.manager().edit(montant);
            } catch (NonexistentEntityException ex) {
                // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    //Query delete rayon where id=super.getIdADelete()
    public void delete() {
        try {
            Rayons.manager().destroy(super.getIdADelete());
            //refreshPage();
        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            // Logger.getLogger(RayonBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("La suppression a échouée");
        }
    }

}
