/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans;

import bluemarket.shopeasy.admin.request.StaticContainerManager;

/**
 *
 * @author David
 */
public abstract class MasterBean{
    
    private int idADelete;
    private int id;
    private String erreur;

    public MasterBean() {
        StaticContainerManager.url = "http://192.168.128.13/~d120027/shopeasy/web/container";
    }
    
    public int getIdADelete() {
        return idADelete;
    }

    public void setIdADelete(int idADelete) {
        this.idADelete = idADelete;
    }

    public String getErreur() {
        return erreur;
    }

    public void setErreur(String erreur) {
        this.erreur = erreur;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
