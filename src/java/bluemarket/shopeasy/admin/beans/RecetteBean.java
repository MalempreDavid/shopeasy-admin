
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Produits;
import be.bluemarket.shopeasy.database.jpa.controllers.Recettes;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.IllegalOrphanException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.NonexistentEntityException;
import be.bluemarket.shopeasy.database.models.Recette;
import be.bluemarket.shopeasy.database.models.Composer;
import be.bluemarket.shopeasy.database.models.Produit;
import bluemarket.shopeasy.admin.request.StaticContainerManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

/**
 *
 * @author David
 */
@ManagedBean
@ViewScoped
public class RecetteBean extends MasterBean {

    private Recette currentRecette;
    private Composer unIngredient;
    private Part image;

    public RecetteBean() {
        currentRecette = new Recette();
        currentRecette.setIngredients(new ArrayList<Composer>());
        resetIngredient();
    }

    private void resetIngredient() {
        unIngredient = new Composer();
        Produit produit = new Produit();
        unIngredient.setProduit(produit);
    }

    @PostConstruct
    private void init() {
        Recettes.manager();
        Produits.manager();
    }

    //Query: r�cup�rer les recettes
    public List<Recette> getRecettes() {
        return Recettes.manager().findEntities();
    }

    public Recette getCurrentRecette() {
        return currentRecette;
    }

    public String[] getCategories() {
        String[] cat = new String[5];
        cat[0] = "--Catégorie--";
        cat[1] = "Entree";
        cat[2] = "Potage";
        cat[3] = "Plats";
        cat[4] = "Dessert";
        return cat;
    }

    //Query: SELECT produit WHERE id = unIngredient.getProduit.getId
    //Stocker r�sultat dans unIngredient.setProduit
    public void ajouter() {
        Produit p = Produits.manager().find(unIngredient.getProduit().getId());
        unIngredient.setProduit(p);
        currentRecette.getIngredients().add(unIngredient);
        resetIngredient();
    }

    public void supprimer(Composer composition) {
        currentRecette.getIngredients().remove(composition);
    }

    public Composer getUnIngredient() {
        return unIngredient;
    }

    public Part getFile() {
        return image;
    }

    public void setFile(Part file) {
        image = file;
    }

    //Query: ajouter currentRecette
    public String ajouterRecette() {
        if (currentRecette.getNom().isEmpty() || currentRecette.getPreparation().isEmpty() || currentRecette.getIngredients().isEmpty()) {
            super.setErreur("Impossible de valider la recette, des champs sont vides");
            return null;
        }
        if(currentRecette.getCategorie().equals("--Catégorie--")){
             super.setErreur("Veuillez choisir une catégorie");
            return null;
        }
        if (image.getName().isEmpty()) {
            super.setErreur("Impossible de valider la recette, pas d'image");
            return null;
        }

        try {
            String url = askUrl(image);
            currentRecette.setUrlImage(url);
            Recettes.manager().create(currentRecette);
            return "recette";

        } catch (Exception ex) {
            Logger.getLogger(RecetteBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'ajout a échoué");
            return null;
        }
    }

    private String askUrl(Part image) throws Exception {
        StaticContainerManager scm = new StaticContainerManager(currentRecette);
        String url = scm.enregistrer(image);
        image.delete();
        return url;
    }

    //Query: delete recette where id = super.getIdADelete
    public void delete() {
        try {
            Recette recette = Recettes.manager().find(super.getIdADelete());
            StaticContainerManager scm = new StaticContainerManager(recette);
            if (scm.supprimer()) {
                Recettes.manager().destroy(super.getIdADelete());
            }

        } catch (IllegalOrphanException | NonexistentEntityException ex) {
            // Logger.getLogger(RecetteBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
           // Logger.getLogger(RecetteBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Query: SELECT recette WHERE id = recette.id
    //A stocker dans currentRecette
    public void setAllFieldEdition() {
        currentRecette = Recettes.manager().find(currentRecette.getId());

        if (currentRecette == null) {
            try {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.getExternalContext().redirect("recette.xhtml");
            } catch (IOException ex) {
                Logger.getLogger(ProduitBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    //Query: update currentRecette
    public String update() {
        if (currentRecette.getNom().isEmpty() || currentRecette.getPreparation().isEmpty() || currentRecette.getIngredients().isEmpty()) {
            super.setErreur("Impossible de valider la recette, des champs sont vides");
            return null;
        }

        try {
            String url = askUrl(image);
            currentRecette.setUrlImage(url);
            Recettes.manager().edit(currentRecette);
            return "recette";
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RecetteBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("Cette recette n'existe pas (ou plus), elle ne peut être éditée");
            return null;
        } catch (Exception ex) {
            Logger.getLogger(RecetteBean.class.getName()).log(Level.SEVERE, null, ex);
            super.setErreur("L'édition a échouée");
            return null;
        }
    }
}
