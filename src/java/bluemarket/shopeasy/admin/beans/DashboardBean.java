/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Produits;
import be.bluemarket.shopeasy.database.jpa.controllers.Rayons;
import be.bluemarket.shopeasy.database.jpa.controllers.Recettes;
import be.bluemarket.shopeasy.database.jpa.controllers.Utilisateurs;
import be.bluemarket.shopeasy.database.models.Produit;
import be.bluemarket.shopeasy.database.models.Utilisateur;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author David
 */
@ManagedBean
public class DashboardBean {

    @PostConstruct
    public void init() {
        Utilisateurs.manager();
        Rayons.manager();
        Produits.manager();
        Recettes.manager();
    }

    //Query: count users
    public int getUsers() {
        return Utilisateurs.manager().count();
    }

    //Query: count rayons
    public int getRayons() {
        return Rayons.manager().count();
    }

    //Query: count produits
    public int getProduits() {
        return Produits.manager().count();
    }

    //Query: count recettes
    public int getRecettes() {
        return Recettes.manager().count();
    }

    //Query: obtenir 10 produits les plus listés
    public List<Produit> getTopProduits() {
        return Produits.manager().getTop(0, 10);
    }

    //Query: obtenir 10 derniers utilisateurs enregistrés
    public List<Utilisateur> getLastUtilisateur() {
        return Utilisateurs.manager().findEntities(Utilisateur.Order.Inscription, 10, 0);
    }

}
