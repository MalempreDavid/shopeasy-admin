package bluemarket.shopeasy.admin.beans;

import be.bluemarket.shopeasy.database.jpa.controllers.Utilisateurs;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.WrongAttributeNameException;
import be.bluemarket.shopeasy.database.jpa.controllers.exceptions.WrongAttributeTypeException;
import be.bluemarket.shopeasy.database.models.Utilisateur;
import hermes.security.hash.Hasher;
import hermes.security.hash.HasherFactory;
import hermes.security.hash.HmacHasher;
import hermes.security.hash.SHAHasher;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LoginBean extends MasterBean {
    
    private HmacHasher hasher;

    private Utilisateur utilisateur;

    private Utilisateur sessionsUtilisateur;

    @PostConstruct
    public void init() {
        Utilisateurs.manager();
        utilisateur = new Utilisateur();
        hasher = (HmacHasher) HasherFactory.make(HasherFactory.HMAC);
        hasher.setAlgorithm(HmacHasher.SHA512);
        hasher.setSalt("SWAG");
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    // Query: select where utilisateur = utilisateur.getPseudo()
    public String login() {
        if (!verifierChamps()) {
            return "";
        }
        String location = "";
        Utilisateur utilisateurQuery;
        try {
            utilisateurQuery = Utilisateurs.manager().find("login", utilisateur.getLogin());
            if (!utilisateurQuery.getRole().equals("Administrateur") && !utilisateurQuery.getRole().equals("SuperAdmin")) {
                super.setErreur("Vous n'avez pas les droits requis");
                location = "";
            } else if (comparerPassword(utilisateur.getPassword(),utilisateurQuery.getPassword())) {
                sessionsUtilisateur = utilisateurQuery;
                location = "/Administration/index";
            } else {
                super.setErreur("Pseudo ou mot de passe invalide");
                location = "";
            }
        } catch (WrongAttributeTypeException | WrongAttributeNameException ex) {
            // Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return location;
    }
    
    private boolean comparerPassword(String password, String hashed) {
        return hasher.hash(password).equals(hashed);
    }

    private boolean verifierChamps() {
        if (utilisateur.getLogin().isEmpty()) {
            super.setErreur("Le champs pseudo doit être rempli !");
            return false;
        }
        if (utilisateur.getPassword().isEmpty()) {
            super.setErreur("Le champs mot de passe doit être rempli !");
            return false;
        }

        return true;
    }

    public void deconnexion() {
        sessionsUtilisateur = null;
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            ctx.getExternalContext().redirect("login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Utilisateur getSessionsUtilisateur() {
        return sessionsUtilisateur;
    }

}
